﻿using System;

using Xamarin.Forms;
using XFTest.ViewPages;

namespace XFTest.Views
{
	public class FirstPageView : ContentView
	{
		public Label txtTitle;
		public Button btnNextPage,btnPrevPage;
		public FirstPageView ()
		{
			var MainContainer =	new StackLayout {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Vertical,
				Spacing = 2,
				//Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10,50,10,10)
			};

			this.txtTitle = new Label {
				XAlign = TextAlignment.Center,
				Text = "Welcome to Xamarin Forms!"
			};
			this.btnNextPage = new Button {
				Text = "Load Next...",
				BackgroundColor = Color.FromHex("#FFF2F2F2"),
				TextColor = Color.Black,
				IsEnabled = true,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 45
			};

			this.btnPrevPage = new Button {
				Text = "Load Prev...",
				BackgroundColor = Color.FromHex("#FFCCCCCC"),
				TextColor = Color.Black,
				IsEnabled = false,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 45
			};

			MainContainer.Children.Add (this.txtTitle);
			MainContainer.Children.Add (this.btnNextPage);
			MainContainer.Children.Add (this.btnPrevPage);
			this.btnNextPage.Clicked += async (object sender, EventArgs e) => {
				System.Diagnostics.Debug.WriteLine("Goto Next Page");
				await Navigation.PushAsync(new SecondPage(), true);
			};

			this.btnPrevPage.Clicked += (object sender, EventArgs e) => {
				System.Diagnostics.Debug.WriteLine("Goto Prev Page");
				this.Navigation.PopAsync();
			};

			// This is how to do Gesture Recognizer
			var titleTouch = new TapGestureRecognizer();
			titleTouch.NumberOfTapsRequired = 2;
			titleTouch.Tapped += this.DisplayConsole;
			this.txtTitle.GestureRecognizers.Add(titleTouch);

			Content = MainContainer;
		}

		public void DisplayConsole(object sender, EventArgs e){
			System.Diagnostics.Debug.WriteLine ("txtTitle is tapped...");
		}
	}
}


