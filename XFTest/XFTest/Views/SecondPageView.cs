﻿using System;

using Xamarin.Forms;

namespace XFTest
{
	public class SecondPageView : ContentView
	{
		public Label txtTitle;
		public Button btnNextPage,btnPrevPage;
		public Grid sixgrid;
		public Image t1, t2, t3, t4, t5;
		public TapGestureRecognizer taptap1,taptap2,taptap3,taptap4,taptap5;
		public Label tapresult;

		public SecondPageView ()
		{
				var Maincontainer = new StackLayout 
				{

				};

				this.sixgrid = new Grid // Declare grid size
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					RowDefinitions =
					{
						new RowDefinition { Height = new GridLength(1, GridUnitType.Star)},
						new RowDefinition { Height = new GridLength(1, GridUnitType.Star)},
						new RowDefinition { Height = new GridLength(1, GridUnitType.Star)}
					},
					ColumnDefinitions = 
					{
						new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)},
						new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
					}
					};

				this.t1 = new Image // Set image with image source
				{
					Source = ImageSource.FromFile("leonardo.png"),
				};

				this.t2 = new Image 
				{
					Source = ImageSource.FromFile("raphael.png")
				};

				this.t3 = new Image 
				{
					Source = ImageSource.FromFile("donatello.png")
				};

				this.t4 = new Image 
				{
					Source = ImageSource.FromFile("michelangelo.png")
				};

				this.t5 = new Image 
				{
					Source = ImageSource.FromFile("assembly.png")
				};

				this.tapresult = new Label 
				{
					Text = "Hi, please click the picture."
				};

				this.taptap1 = new TapGestureRecognizer {
					//NumberOfTapsRequired = 1
				};

				this.taptap2 = new TapGestureRecognizer {
					//NumberOfTapsRequired = 1
				};

				this.taptap3 = new TapGestureRecognizer {
					//NumberOfTapsRequired = 1
				};

				this.taptap4 = new TapGestureRecognizer {
					//NumberOfTapsRequired = 1
				};
				this.taptap5 = new TapGestureRecognizer {
					NumberOfTapsRequired = 2
				};

				this.sixgrid.Children.Add ((t1), 0, 0 ); // Add image to grid with grid position
				this.sixgrid.Children.Add ((t2), 1, 0 ); // ((image), column, row)
				this.sixgrid.Children.Add ((t3), 0, 1 );
				this.sixgrid.Children.Add ((t4), 1, 1 );
				this.sixgrid.Children.Add ((t5), 0, 2 );
				this.sixgrid.Children.Add ((tapresult), 1, 2);

				this.taptap1.Tapped += delegate // Event handler for tap gesture recognization
				{
					tapresult.Text = "Nice to meet you, I am the leader- Leonardo";
				};

				this.taptap2.Tapped += delegate 
				{
					tapresult.Text = "Hey, I am the stronges- Raphael";
				};

				this.taptap3.Tapped += delegate 
				{
					tapresult.Text = "Hi, I am the technician- Donatello";
				};

				this.taptap4.Tapped += delegate 
				{
					tapresult.Text = "Halo everybody, I am Mikey, the youngest";
				};

				this.taptap5.Tapped += delegate 
				{
					tapresult.Text = "And we are, Teenage Mutant Ninja Turtle (TMNT)!!!";
				};

				//			this.sixgrid.Children.Add(new Image
				//			{
				////				Text = "1",
				////				HorizontalOptions = LayoutOptions.Center
				//				Source = ImageSource.FromFile("leonardo.png")
				//			}, 0, 0); // column, row

				//			this.sixgrid.Children.Add(new Image
				//			{
				////				Text = "2",
				////				HorizontalOptions = LayoutOptions.Center
				//				Source = ImageSource.FromFile("raphael.png")
				//			}, 1, 0); // column, row
				//			
				//			this.sixgrid.Children.Add(new Image
				//			{
				////				Text = "3",
				////				HorizontalOptions = LayoutOptions.Center
				//				Source = ImageSource.FromFile("donatello.png")
				//			}, 0, 1); // column, row
				//
				//			this.sixgrid.Children.Add(new Image
				//			{
				////				Text = "4",
				////				HorizontalOptions = LayoutOptions.Center
				//				Source = ImageSource.FromFile("michelangelo.png")
				//			}, 1, 1); // column, row
				//
				//			this.sixgrid.Children.Add(new Image
				//			{
				////				Text = "5",
				////				HorizontalOptions = LayoutOptions.Center
				//				Source = ImageSource.FromFile("assembly.png")
				//			}, 0, 2); // column, row

				//			this.sixgrid.Children.Add(new Label
				//			{
				//				Text = "Click the Ninja Turtle",
				//				HorizontalOptions = LayoutOptions.Center
				//			}, 1, 2); // column, row

				t1.GestureRecognizers.Add (taptap1);
				t2.GestureRecognizers.Add (taptap2);
				t3.GestureRecognizers.Add (taptap3);
				t4.GestureRecognizers.Add (taptap4);
				t5.GestureRecognizers.Add (taptap5);
				Maincontainer.Children.Add (sixgrid);
				Content = Maincontainer;
		}
	}
}


