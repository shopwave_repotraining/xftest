﻿using System;

using Xamarin.Forms;
using XFTest.Views;

namespace XFTest.ViewPages
{
	public class SecondPage : ContentPage
	{
		public SecondPage ()
		{
			Content = new SecondPageView ();
		}
	}
}


